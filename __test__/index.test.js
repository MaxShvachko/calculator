var calculator = require("../client/script");
var jsdom = require("jsdom");
jest.mock("../client/script")

var {JSDOM} = jsdom;
var dom = new JSDOM(`<!DOCTYPE html><html lang="en"><head></head><body></body></html>`);

displayValue = calculator.displayValue;
clearDisplay = calculator.clearDisplay;

describe('displayValue check displayValue', () => {
  var btn7 = dom.window.document.createElement("button");
  var display = dom.window.document.createElement("input");

  dom.window.document.body.append(btn7);
  dom.window.document.body.append(display);

  test('displayValue is', () => {
    btn7 = 5;
    var expected = display = 7
    var action =  displayValue(btn7);
    expect(action).toBe(expected);
  });
});

describe('clearDisplay', () => {
  var clear = dom.window.document.createElement("button");
  var display = dom.window.document.createElement("input");

  dom.window.document.body.append(clear);
  dom.window.document.body.append(display);

  test('clearDisplay clean 123 to "" ', () => {
    display.value = "123";
    var expected = display.value = "";
    var action =  clearDisplay();
    expect(action).toBe(expected);
  });
});