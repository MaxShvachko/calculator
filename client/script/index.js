var display = document.getElementById("display");
var clear = document.getElementById("clear");
var btn7 = document.getElementById("btn7");
var btn4 = document.getElementById("btn4");
var btn1 = document.getElementById("btn1");
var btn0 = document.getElementById("btn0");
var bracket1 = document.getElementById("bracket1");
var btn8 = document.getElementById("btn8");
var btn5 = document.getElementById("btn5");
var btn2 = document.getElementById("btn2");
var bracket2 = document.getElementById("bracket2");
var btn9 = document.getElementById("btn9");
var btn6 = document.getElementById("btn6");
var btn3 = document.getElementById("btn3");
var btnComa = document.getElementById("btnComa");
var btnDivide = document.getElementById("btnDivide");
var btnMultiply = document.getElementById("btnMultiply");
var btnMinus = document.getElementById("btnMinus");
var btnPlus = document.getElementById("btnPlus");
var btnEqually = document.getElementById("btnEqually");

window.onload=function(){
  btn7.addEventListener('click', function(){displayValue(7)});
  btn4.addEventListener('click', function(){displayValue(4)});
  btn1.addEventListener('click', function(){displayValue(1)});
  btn8.addEventListener('click', function(){displayValue(8)});
  btn5.addEventListener('click', function(){displayValue(5)});
  btn2.addEventListener('click', function(){displayValue(2)});
  btn0.addEventListener('click', function(){displayValue(0)});
  btn9.addEventListener('click', function(){displayValue(9)});
  btn6.addEventListener('click', function(){displayValue(6)});
  btn3.addEventListener('click', function(){displayValue(3)});
  btnComa.addEventListener('click', function(){displayValue(".")});
  bracket1.addEventListener('click', function(){displayValue("(")});
  bracket2.addEventListener('click', function(){displayValue(")")});
  btnDivide.addEventListener('click', function(){displayValue("/")});
  btnMultiply.addEventListener('click', function(){displayValue("*")});
  btnMinus.addEventListener('click', function(){displayValue("-")});
  btnPlus.addEventListener('click', function(){displayValue("+")});
  clear.addEventListener('click', clearDisplay);
  btnEqually.addEventListener('click', equal);
};


//Value of buttons
// var value7 = btn7.value = 7;
// var value4 = btn4.value = 4;
// var value1 = btn1.value = 1;
// var value0 = btn0.value = 0;
// var value8 = btn8.value = 8;
// var value5 = btn5.value = 5;
// var value2 = btn2.value = 2;
// var value9 = btn9.value = 9;
// var value6 = btn6.value = 6;
// var value3 = btn3.value = 3;
// var valueComa = btnComa.value = .0;
// var valueBracket1 = bracket1.value = '(';
// var valueBracket2 = bracket2.value = ')';

function displayValue(value) {
  if (display.value === "." || display.value.includes(".")) {
    return;
  }
  display.value = display.value + value;
}

function clearDisplay() {
  display.value = "";
}

function equal() {
  var expression = display.value;
  display.value = eval(expression);
}

module.exports = {
  displayValue: displayValue,
  clearDisplay: clearDisplay
}




